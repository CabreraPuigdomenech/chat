Esteve Cabrera Quipu App
=========================

## Uso

"npm install && npm start"

## Arquitectura

Para la arquitectura de la aplicación,
no he querido utilizar redux porque añadir una biblioteca
densa para un proyecto tan pequeño tiene poco uso, aun asi
si la hubiera utilizado, hubiera creado las acciones y los
reducers pertinentes.

La pagina principal HTML esta en public/index.html, y el 
fichero de entrada esta en src/index.js, eso llama al 
Componente src/App.js, tenemos un componente principal,
que contiene los dos Componentes( siendo el mismo componente)
de chat, el cual contiene la parte de enviado y de vista del 
chat. Ademas hay un componente que hace del Dialog para el
extra ball.

Para los estilos y la visualización he utilizado la biblioteca
de Material-UI, por la experiencia que tengo utilizandola.

## Consideraciones

No he utilizado un sistema de routing, para la aplicacion , porque 
no he visto el sentido de ponerle, routing a una sola  pagina, si 
hiciera falta , añadiria la biblioteca react-router.

Lo mismo con redux, no creo que haga falta para esta aplicación,
Como he explicado antes, aun asi si la tuviera que utilizar, instalaria 
redux y crearia una carpeta para redux con sus acciones y reducers.

## Ampliaciones posibles

1. Poder borrar los mensajes.
2. Añadir una mejora de visualización del contenido.
3. Mejorar los test.
4. Añadir algun listener que faltaria para mejorar la interacción

Gracias por la oportunidad!