import React from "react";
import ChatViewComponent from "../Components/ChatViewComponent";
import { mount } from "enzyme";
import uuidv4 from "uuid/v4";
import Adapter from "enzyme-adapter-react-16";
import { configure } from "enzyme";
configure({ adapter: new Adapter() });

const userTest = {
  name: "Roger",
  id: uuidv4()
};

describe("ChatViewComponent", () => {
  let pushMessage = null;
  let wrapper = null;
  beforeEach(() => {
    pushMessage = jest.fn();
    wrapper = mount(
      <ChatViewComponent
        user={userTest}
        messages={[]}
        handlePushMessage={pushMessage}
      />
    );
  });
  it("initial props", () => {
    expect(wrapper.props().user).toEqual(userTest);
    expect(wrapper.props().messages).toEqual([]);
  });
});
