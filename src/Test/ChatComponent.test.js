import React from "react";
import ChatComponent from "../Components/ChatComponent";
import { mount } from "enzyme";
import uuidv4 from "uuid/v4";
import Adapter from "enzyme-adapter-react-16";
import { configure } from "enzyme";
configure({ adapter: new Adapter() });

const userTest = {
  name: "Roger",
  id: uuidv4()
};

describe("ChatComponent", () => {
  let pushMessage = null;
  let wrapper = null;
  beforeEach(() => {
    pushMessage = jest.fn();
    wrapper = mount(
      <ChatComponent
        user={userTest}
        messages={[]}
        handlePushMessage={pushMessage}
      />
    );
  });
  it("initial props", () => {
    expect(wrapper.props().user).toEqual(userTest);
    expect(wrapper.props().messages).toEqual([]);
  });
  it("initial state", () => {
    const messageToPush = {
      text: "test",
      date: expect.anything(),
      user: wrapper.childAt(0).props().user.name,
      userId: wrapper.childAt(0).props().user.id
    };
    wrapper
      .childAt(0)
      .instance()
      .handleSendMessage("test");
    expect(wrapper.childAt(0).props().handlePushMessage).toHaveBeenCalledWith(
      messageToPush
    );
  });
});
