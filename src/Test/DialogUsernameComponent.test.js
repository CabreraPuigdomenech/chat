import React from "react";
import DialogUsernameComponent from "../Components/DialogUsernameComponent";
import { mount } from "enzyme";
import uuidv4 from "uuid/v4";
import Adapter from "enzyme-adapter-react-16";
import { configure } from "enzyme";
configure({ adapter: new Adapter() });

const userTest = {
  name: "Roger",
  id: uuidv4()
};

describe("DialogUsernameComponent", () => {
  let close = null;
  let accept = null;
  let wrapper = null;
  beforeEach(() => {
    close = jest.fn();
    accept = jest.fn();
    wrapper = mount(
      <DialogUsernameComponent
        isOpen={true}
        handleClose={close}
        handleAccept={accept}
      />
    );
  });
  it("initial props", () => {
    expect(wrapper.props().isOpen).toEqual(true);
  });
  it("initial state", () => {
    expect(wrapper.state().nameLeft).toEqual("");
    expect(wrapper.state().nameRight).toEqual("");
  });
  it("changeUsername", () => {
    wrapper
      .instance()
      .onChangeUsername({ target: { id: "nameLeft", value: "test" } });
    wrapper
      .instance()
      .onChangeUsername({ target: { id: "nameRight", value: "test" } });
    expect(wrapper.state().nameLeft).toEqual("test");
    expect(wrapper.state().nameRight).toEqual("test");
  });
});
