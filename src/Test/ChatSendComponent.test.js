import React from "react";
import ChatSendComponent from "../Components/ChatSendComponent";
import { mount } from "enzyme";
import uuidv4 from "uuid/v4";
import Adapter from "enzyme-adapter-react-16";
import { configure } from "enzyme";
configure({ adapter: new Adapter() });

const userTest = {
  name: "Roger",
  id: uuidv4()
};

describe("ChatSendComponent", () => {
  let pushMessage = null;
  let wrapper = null;
  beforeEach(() => {
    pushMessage = jest.fn();
    wrapper = mount(<ChatSendComponent handleSendMessage={pushMessage} />);
  });
  it("initial state", () => {
    expect(wrapper.childAt(0).state().text).toEqual("");
  });
  it("handleEnterMessage state", () => {
    wrapper
      .childAt(0)
      .instance()
      .handleEnterMessage({ key: "Enter", target: { value: "test" } });
    expect(wrapper.childAt(0).props().handleSendMessage).toHaveBeenCalledTimes(
      1
    );
    expect(wrapper.childAt(0).state().text).toEqual("");
  });
  it("handleSendButton state", () => {
    wrapper.childAt(0).state().text = "test";
    wrapper
      .childAt(0)
      .instance()
      .handleSendButton();
    expect(wrapper.childAt(0).props().handleSendMessage).toHaveBeenCalledTimes(
      1
    );
    expect(wrapper.childAt(0).state().text).toEqual("");
  });
  it("handleChangeMessage state", () => {
    wrapper
      .childAt(0)
      .instance()
      .handleChangeMessage({ target: { value: "test" } });
    expect(wrapper.childAt(0).state().text).toEqual("test");
  });
});
