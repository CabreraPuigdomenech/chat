const formatDate = date => {
  return date.toTimeString().substring(0, 8);
};

export { formatDate };
