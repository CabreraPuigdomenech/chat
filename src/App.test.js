import React from "react";
import QuipuComponent from "./Components/QuipuComponent";

import { mount } from "enzyme";
import uuidv4 from "uuid/v4";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

const userTest = {
  name: "Roger",
  id: uuidv4()
};

describe("QuipuComponent", () => {
  let wrapper = null;
  beforeEach(() => {
    wrapper = mount(<QuipuComponent />);
  });
  it("initial state", () => {
    expect(wrapper.state().isOpen).toEqual(true);
    expect(wrapper.state().userLeft.name).toEqual("Left");
    expect(wrapper.state().userLeft.id).toMatch(
      new RegExp(
        "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
      )
    );
    expect(wrapper.state().userRight.name).toEqual("Right");
    expect(wrapper.state().userRight.id).toMatch(
      new RegExp(
        "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
      )
    );
    expect(wrapper.state().messages).toEqual([]);
  });
  it("handleAccept", () => {
    wrapper.instance().handleAccept("Roger", "Lluis");
    expect(wrapper.state().isOpen).toEqual(false);
    expect(wrapper.state().userRight.name).toEqual("Lluis");
    expect(wrapper.state().userLeft.name).toEqual("Roger");
  });
  it("handleClose", () => {
    wrapper.instance().handleClose();
    expect(wrapper.state().isOpen).toEqual(false);
  });
  it("pushMessage", () => {
    wrapper
      .instance()
      .pushMessage({ date: new Date(), name: "roger", userId: uuidv4() });
    expect(wrapper.state().messages.length).toEqual(1);
  });
});
