import React, { Component, Fragment } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Paper from "@material-ui/core/Paper";
import ListItemText from "@material-ui/core/ListItemText";

import chatViewComponent from "../Assets/chatViewComponent";
import { formatDate } from "../Helpers/DateHelper";

class ChatViewComponent extends Component {
  messagesEnd = React.createRef();

  componentDidUpdate() {
    const { messages, user } = this.props;
    if (
      messages.length !== 0 &&
      messages[messages.length - 1].userId === user.id
    ) {
      this.scrollToBottom();
    }
  }

  scrollToBottom() {
    this.messagesEnd.current.scrollIntoView({ behavior: "smooth" });
  }

  render() {
    const { classes, messages, user } = this.props;
    const listMessages = messages.map(message => {
      let style = { backgroundColor: "lightblue", marginRight: "30%" };

      if (message.userId === user.id) {
        style = { backgroundColor: "lightgreen", marginLeft: "30%" };
      }
      return (
        <ListItem
          className={classes.listitem}
          style={style}
          key={"listItem" + message.user + message.date}
        >
          <ListItemText
            key={"listItemText" + message.user + message.date}
            primary={
              <Fragment>
                <h5
                  key={"h5name" + message.user + message.date}
                  className={classes.h5}
                >
                  {message.user}:
                </h5>
                <h5
                  key={"h5date" + message.user + message.date}
                  className={classes.h5time}
                >
                  {formatDate(message.date)}
                </h5>
              </Fragment>
            }
            secondary={message.text}
            classes={classes}
          />
        </ListItem>
      );
    });
    return (
      <Paper className={classes.paper}>
        <List>{listMessages}</List>
        <div ref={this.messagesEnd} />
      </Paper>
    );
  }
}

ChatViewComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  messages: PropTypes.array.isRequired,
  user: PropTypes.object.isRequired
};

export default withStyles(chatViewComponent)(ChatViewComponent);
