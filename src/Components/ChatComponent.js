import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper";
import withStyles from "@material-ui/core/styles/withStyles";
import ChatViewComponent from "./ChatViewComponent";
import ChatSendComponent from "./ChatSendComponent";
import chatComponentStyle from "../Assets/chatComponentStyle";

class ChatComponent extends Component {
  handleSendMessage = text => {
    const { handlePushMessage, user } = this.props;
    const message = {
      text,
      date: new Date(),
      user: user.name,
      userId: user.id
    };
    handlePushMessage(message);
  };

  render() {
    const { classes, user, messages } = this.props;
    return (
      <Fragment>
        <Paper className={classes.paper}>
          <h4>Ventana de chat de: {user.name}</h4>
          <ChatViewComponent user={user} messages={messages} />
          <ChatSendComponent handleSendMessage={this.handleSendMessage} />
        </Paper>
      </Fragment>
    );
  }
}

ChatComponent.propType = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  handlePushMessage: PropTypes.func.isRequired,
  messages: PropTypes.array.isRequired
};

export default withStyles(chatComponentStyle)(ChatComponent);
