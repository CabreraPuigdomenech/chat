import React, { Component } from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

class DialogUsernameComponent extends Component {
  state = {
    nameLeft: "",
    nameRight: ""
  };

  onChangeUsername = event => {
    const { id, value } = event.target;
    this.setState({
      [id]: value
    });
  };

  render() {
    const { isOpen, handleClose, handleAccept } = this.props;
    const { nameLeft, nameRight } = this.state;
    return (
      <Dialog
        open={isOpen}
        onClose={this.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Escribe un nombre de usuario
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            El valor por defecto del nombre de usuario es "Left" para la ventana
            de la izquierda o "Right" para la derecha, si lo deseas puedes
            cambiar los nombres
          </DialogContentText>
          <TextField
            margin="dense"
            id="nameLeft"
            label="Nombre usuario izquierda"
            fullWidth
            onChange={this.onChangeUsername}
          />
          <TextField
            margin="dense"
            id="nameRight"
            label="Nombre usuario derecha"
            fullWidth
            onChange={this.onChangeUsername}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancelar
          </Button>
          <Button
            onClick={() => handleAccept(nameLeft, nameRight)}
            color="primary"
            variant="contained"
          >
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

DialogUsernameComponent.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  handleAccept: PropTypes.func.isRequired
};

export default DialogUsernameComponent;
