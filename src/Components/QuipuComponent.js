import React, { Component, Fragment } from "react";
import uuidv4 from "uuid/v4";
import ChatComponent from "./ChatComponent";
import DialogUsernameComponent from "./DialogUsernameComponent";

class QuipuComponent extends Component {
  state = {
    isOpen: true,
    userLeft: { name: "Left", id: uuidv4() },
    userRight: { name: "Right", id: uuidv4() },
    messages: []
  };

  handleAccept = (userLeft, userRight) => {
    this.setState({
      isOpen: false,
      userLeft: { name: userLeft !== "" ? userLeft : "Left", id: uuidv4() },
      userRight: { name: userRight !== "" ? userRight : "Right", id: uuidv4() }
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false
    });
  };

  pushMessage = objectMessage => {
    this.setState(prevState => ({
      messages: [...prevState.messages, objectMessage]
    }));
  };

  render() {
    const { isOpen, userLeft, userRight, messages } = this.state;
    return (
      <Fragment>
        <ChatComponent
          user={userLeft}
          handlePushMessage={this.pushMessage}
          messages={messages}
        />
        <ChatComponent
          user={userRight}
          handlePushMessage={this.pushMessage}
          messages={messages}
        />
        <DialogUsernameComponent
          isOpen={isOpen}
          handleAccept={this.handleAccept}
          handleClose={this.handleClose}
        />
      </Fragment>
    );
  }
}

export default QuipuComponent;
