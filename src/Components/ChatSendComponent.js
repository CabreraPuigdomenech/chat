import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Send from "@material-ui/icons/Send";
import chatSendComponentStyle from "../Assets/chatSendComponentStyle";

class ChatSendComponent extends Component {
  state = {
    text: ""
  };

  handleEnterMessage = event => {
    const { value } = event.target;
    if (event.key === "Enter" && value !== "") {
      this.props.handleSendMessage(value);
      this.setState({
        text: ""
      });
    }
  };

  handleSendButton = event => {
    const { text } = this.state;
    if (text !== "") {
      this.props.handleSendMessage(text);
      this.setState({
        text: ""
      });
    }
  };

  handleChangeMessage = event => {
    const { value } = event.target;
    this.setState({
      text: value
    });
  };

  render() {
    const { classes } = this.props;
    const { text } = this.state;
    return (
      <Paper className={classes.paper}>
        <div style={{ width: "69%", float: "left" }}>
          <TextField
            fullWidth
            onKeyPress={this.handleEnterMessage}
            onChange={this.handleChangeMessage}
            value={text}
          />
        </div>
        <div style={{ width: "29%", float: "right" }}>
          <Button
            className={classes.buttonSend}
            id="sendButton"
            variant="contained"
            color="primary"
            onClick={this.handleSendButton}
          >
            Enviar
            <Send />
          </Button>
        </div>
      </Paper>
    );
  }
}

ChatSendComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  handleSendMessage: PropTypes.func.isRequired
};

export default withStyles(chatSendComponentStyle)(ChatSendComponent);
