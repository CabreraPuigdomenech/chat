const chatComponentStyle = {
  paper: {
    margin: "4%",
    marginBottom: "0px",
    width: "40%",
    float: "left",
    padding: "1%"
  }
};

export default chatComponentStyle;
