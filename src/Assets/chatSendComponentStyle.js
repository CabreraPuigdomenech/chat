const chatSendComponentStyle = {
  paper: {
    padding: "1%",
    height: "40px"
  },
  buttonSend: {
    float: "right"
  }
};

export default chatSendComponentStyle;
