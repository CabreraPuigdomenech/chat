const chatViewComponent = {
  paper: {
    marginTop: "3%",
    marginBottom: "3%",
    maxHeight: "600px",
    height: "600px",
    overflowY: "auto"
  },
  h5: {
    margin: "0px"
  },
  h5time: {
    margin: "0px",
    float: "right"
  },
  listitem: {
    borderRadius: "10px",
    paddingRight: "0px",
    margin: "1%",
    width: "68%",
    overflowX: "auto"
  }
};

export default chatViewComponent;
