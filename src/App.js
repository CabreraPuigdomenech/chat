import React, { Component } from "react";
import QuipuComponent from "./Components/QuipuComponent";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { grey, green } from "@material-ui/core/colors";

const theme = createMuiTheme({
  palette: {
    primary: green,
    secondary: grey
  }
});
class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <QuipuComponent />
      </MuiThemeProvider>
    );
  }
}

export default App;
